var group__config_router =
[
    [ "otGetJoinerUdpPort", "da/d96/group__config-router.html#ga13a7243b3d4c16cfc50bfbf29c42dd10", null ],
    [ "otGetLocalLeaderPartitionId", "da/d96/group__config-router.html#ga9a7952895493d3ccce9c1561c4681b60", null ],
    [ "otGetLocalLeaderWeight", "da/d96/group__config-router.html#ga082d2739d97c2a1f465452639fcf7146", null ],
    [ "otSetJoinerUdpPort", "da/d96/group__config-router.html#ga1a81bcce900d21d72fe374af1f756956", null ],
    [ "otSetLocalLeaderPartitionId", "da/d96/group__config-router.html#gaf32beef6568f3e75e3aa1078f61d55c2", null ],
    [ "otSetLocalLeaderWeight", "da/d96/group__config-router.html#ga9188e5b7f239b286e0322acdc84345ae", null ]
];