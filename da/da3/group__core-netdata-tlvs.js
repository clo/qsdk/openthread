var group__core_netdata_tlvs =
[
    [ "NetworkDataTlv", "d5/d81/classThread_1_1NetworkData_1_1NetworkDataTlv.html", [
      [ "Type", "d5/d81/classThread_1_1NetworkData_1_1NetworkDataTlv.html#a21bf7963c4b206ccaa797c431831f9c1", [
        [ "kTypeHasRoute", "d5/d81/classThread_1_1NetworkData_1_1NetworkDataTlv.html#a21bf7963c4b206ccaa797c431831f9c1a3dc3074239a1994272346630de327b9d", null ],
        [ "kTypePrefix", "d5/d81/classThread_1_1NetworkData_1_1NetworkDataTlv.html#a21bf7963c4b206ccaa797c431831f9c1a3cd5c87eab47ce8494682b07f35e9fb2", null ],
        [ "kTypeBorderRouter", "d5/d81/classThread_1_1NetworkData_1_1NetworkDataTlv.html#a21bf7963c4b206ccaa797c431831f9c1a7fa976557b76770546d06d4577e4c9f9", null ],
        [ "kTypeContext", "d5/d81/classThread_1_1NetworkData_1_1NetworkDataTlv.html#a21bf7963c4b206ccaa797c431831f9c1aff7ce45144c62e6c62f9504e1134e7d0", null ],
        [ "kTypeCommissioningData", "d5/d81/classThread_1_1NetworkData_1_1NetworkDataTlv.html#a21bf7963c4b206ccaa797c431831f9c1a4646a91cf3b6ea2bb2c00b0d7ae29484", null ]
      ] ],
      [ "ClearStable", "d5/d81/classThread_1_1NetworkData_1_1NetworkDataTlv.html#aa4859aa514ee48c935c7af7bb7b2f7fa", null ],
      [ "GetLength", "d5/d81/classThread_1_1NetworkData_1_1NetworkDataTlv.html#ae8fa9bac54dd256c88ccc5df8e3b0ef8", null ],
      [ "GetNext", "d5/d81/classThread_1_1NetworkData_1_1NetworkDataTlv.html#af812881954a5608981bfa430eca30731", null ],
      [ "GetType", "d5/d81/classThread_1_1NetworkData_1_1NetworkDataTlv.html#a30c105cfada5eeffd6a9f99c9187ee84", null ],
      [ "GetValue", "d5/d81/classThread_1_1NetworkData_1_1NetworkDataTlv.html#a48c613c8f16703f3af0480e4d4c588b9", null ],
      [ "Init", "d5/d81/classThread_1_1NetworkData_1_1NetworkDataTlv.html#aac4e144713568670ae504b4d40bbcc5d", null ],
      [ "IsStable", "d5/d81/classThread_1_1NetworkData_1_1NetworkDataTlv.html#acf2d4444312a7b915bfd8005aee13fa8", null ],
      [ "SetLength", "d5/d81/classThread_1_1NetworkData_1_1NetworkDataTlv.html#a317f61172c3e2a2c6e03caa4921014d5", null ],
      [ "SetStable", "d5/d81/classThread_1_1NetworkData_1_1NetworkDataTlv.html#af2986c36cf84c021c3a7920af0e9b901", null ],
      [ "SetType", "d5/d81/classThread_1_1NetworkData_1_1NetworkDataTlv.html#a16dd006580876eac0a702c12b6f9db81", null ]
    ] ],
    [ "HasRouteEntry", "da/dc3/classThread_1_1NetworkData_1_1HasRouteEntry.html", [
      [ "GetPreference", "da/dc3/classThread_1_1NetworkData_1_1HasRouteEntry.html#ace41a9cf5541abb3a5e26ef6c43d3bc8", null ],
      [ "GetRloc", "da/dc3/classThread_1_1NetworkData_1_1HasRouteEntry.html#a92d76d1970e97644987b2a75c680d30c", null ],
      [ "Init", "da/dc3/classThread_1_1NetworkData_1_1HasRouteEntry.html#ae3cf5fb627e7d42a228594262d9fc599", null ],
      [ "SetPreference", "da/dc3/classThread_1_1NetworkData_1_1HasRouteEntry.html#a9734a444d27629ca8704f1988dff5638", null ],
      [ "SetRloc", "da/dc3/classThread_1_1NetworkData_1_1HasRouteEntry.html#af292305ab28c84078cca0202895f57bc", null ]
    ] ],
    [ "HasRouteTlv", "db/da1/classThread_1_1NetworkData_1_1HasRouteTlv.html", [
      [ "GetEntry", "db/da1/classThread_1_1NetworkData_1_1HasRouteTlv.html#af793fac409ea244556f3f4690f4b4181", null ],
      [ "GetNumEntries", "db/da1/classThread_1_1NetworkData_1_1HasRouteTlv.html#a2fd02d06c9508d0cba5a95c741f51fd3", null ],
      [ "Init", "db/da1/classThread_1_1NetworkData_1_1HasRouteTlv.html#a62a398f221f165f3513f7b1a8ba39049", null ]
    ] ],
    [ "PrefixTlv", "d0/d48/classThread_1_1NetworkData_1_1PrefixTlv.html", [
      [ "GetDomainId", "d0/d48/classThread_1_1NetworkData_1_1PrefixTlv.html#aa52a1e2f9d25a1e761002685c92f1759", null ],
      [ "GetPrefix", "d0/d48/classThread_1_1NetworkData_1_1PrefixTlv.html#aba35dd736801e79069757eee1d30d204", null ],
      [ "GetPrefixLength", "d0/d48/classThread_1_1NetworkData_1_1PrefixTlv.html#a65883f0127059dc652e37a6b97ac4c07", null ],
      [ "GetSubTlvs", "d0/d48/classThread_1_1NetworkData_1_1PrefixTlv.html#a99a8265aa86b2a1a26340ae1a51421f2", null ],
      [ "GetSubTlvsLength", "d0/d48/classThread_1_1NetworkData_1_1PrefixTlv.html#af03628b4c4e83e968aaebed104496552", null ],
      [ "Init", "d0/d48/classThread_1_1NetworkData_1_1PrefixTlv.html#a3dfaab63612e1c0b225e04688104f7a5", null ],
      [ "SetSubTlvsLength", "d0/d48/classThread_1_1NetworkData_1_1PrefixTlv.html#ae68db3e779a6ef0aaaa9daa28183ae63", null ]
    ] ],
    [ "BorderRouterEntry", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html", [
      [ "kPreferenceOffset", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#abbdd6df09dd819c8704607e851ca5fcda47d0c42046ded9aba718cc5329b6850e", null ],
      [ "kPreferenceMask", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#abbdd6df09dd819c8704607e851ca5fcdab039e9dd5c75b98a942ede4cc6f1a2fc", null ],
      [ "kPreferredFlag", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#abbdd6df09dd819c8704607e851ca5fcda649a7a7b61bf1ff654aaf5a55c1a4061", null ],
      [ "kSlaacFlag", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#abbdd6df09dd819c8704607e851ca5fcda6323504f795e3dea674e59462201cc1f", null ],
      [ "kDhcpFlag", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#abbdd6df09dd819c8704607e851ca5fcda19906167b92771a8e2647fec1e22d4cd", null ],
      [ "kConfigureFlag", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#abbdd6df09dd819c8704607e851ca5fcda454899abe31b85bde0fbe1d9edf1a2b1", null ],
      [ "kDefaultRouteFlag", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#abbdd6df09dd819c8704607e851ca5fcda814ac17790a57f0d2316221fc2db71d0", null ],
      [ "kOnMeshFlag", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#abbdd6df09dd819c8704607e851ca5fcda89a0a8567db8a50d0db8c45b520f1e72", null ],
      [ "ClearConfigure", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#a4b5a94d51657059590f93b1154a4f2ee", null ],
      [ "ClearDefaultRoute", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#aaf07466f2181a24282d6986f5a943fd7", null ],
      [ "ClearDhcp", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#a6457becc9298917d6c134a3dfeedfe47", null ],
      [ "ClearOnMesh", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#acd86e6cac09b7848359ce10cc52bf289", null ],
      [ "ClearPreferred", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#a2f894a14f1f28c1719282a4190d14088", null ],
      [ "ClearSlaac", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#a044a176edbcefd7d8ce23aec2e4e76ae", null ],
      [ "GetFlags", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#a134ba714ed023e6e632a732288991dc6", null ],
      [ "GetPreference", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#ad4e72cfbc88a537f30ee61f6c8176913", null ],
      [ "GetRloc", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#af46474a303cb601ee647ea39660b6a07", null ],
      [ "Init", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#ae719459638bae433026c7bc43e643d96", null ],
      [ "IsConfigure", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#aa0d54ff42613eeb0a4c510535fbe3aed", null ],
      [ "IsDefaultRoute", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#aa9c48016b846b002c858f69a2143a10b", null ],
      [ "IsDhcp", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#a33178a534e67fc122220e28d77f26e64", null ],
      [ "IsOnMesh", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#afee6e69c2d58a79c9c22a93dfcb966ea", null ],
      [ "IsPreferred", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#a295d4e743042a5b995fc222dd286b621", null ],
      [ "IsSlaac", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#ad1294bc438ca13972f93358036200bef", null ],
      [ "SetConfigure", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#a8871e157a23a68f4d296c50c594ce027", null ],
      [ "SetDefaultRoute", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#abe0e36a130c454b8f4a615adef171959", null ],
      [ "SetDhcp", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#a220cdfeb5b67f227f66db66530c6bf36", null ],
      [ "SetFlags", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#a4674e7cd29600273d26da729585176a8", null ],
      [ "SetOnMesh", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#a2a19d33e4ee221ae8e42ce13084fd2b2", null ],
      [ "SetPreference", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#a476693d756cfe0dee6d6f60ede7b2daa", null ],
      [ "SetPreferred", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#a2fdba3655a66a78c058801bb8cf5b4df", null ],
      [ "SetRloc", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#a3a5f9b543ac49d0b7b28c4d07748b771", null ],
      [ "SetSlaac", "d9/d64/classThread_1_1NetworkData_1_1BorderRouterEntry.html#a10d3fc09627f0f5ca5dc7ad2d9093a67", null ]
    ] ],
    [ "BorderRouterTlv", "d2/d89/classThread_1_1NetworkData_1_1BorderRouterTlv.html", [
      [ "GetEntry", "d2/d89/classThread_1_1NetworkData_1_1BorderRouterTlv.html#a245a0e91159fd6178382a7443b906db7", null ],
      [ "GetNumEntries", "d2/d89/classThread_1_1NetworkData_1_1BorderRouterTlv.html#a16b09bf46df7664d73748b4f15017be2", null ],
      [ "Init", "d2/d89/classThread_1_1NetworkData_1_1BorderRouterTlv.html#a3d1275d791b9f61d24d4e1256db6abdc", null ]
    ] ],
    [ "ContextTlv", "df/d56/classThread_1_1NetworkData_1_1ContextTlv.html", [
      [ "ClearCompress", "df/d56/classThread_1_1NetworkData_1_1ContextTlv.html#a11702200453f94f23ed25d876429df4d", null ],
      [ "GetContextId", "df/d56/classThread_1_1NetworkData_1_1ContextTlv.html#a354b3c51fe5e9162a905759392f7a239", null ],
      [ "GetContextLength", "df/d56/classThread_1_1NetworkData_1_1ContextTlv.html#a749270f55d70ce72391410c7e504f215", null ],
      [ "Init", "df/d56/classThread_1_1NetworkData_1_1ContextTlv.html#a113804489e2f9fa8331e3803a0d091a7", null ],
      [ "IsCompress", "df/d56/classThread_1_1NetworkData_1_1ContextTlv.html#ae96bcfdca23efdaf89e9ea4cd2550f81", null ],
      [ "SetCompress", "df/d56/classThread_1_1NetworkData_1_1ContextTlv.html#ae76fc3fc66d06499b9f36caab5f1e599", null ],
      [ "SetContextId", "df/d56/classThread_1_1NetworkData_1_1ContextTlv.html#a436bddda3ee77f005a83f97f35e71bfb", null ],
      [ "SetContextLength", "df/d56/classThread_1_1NetworkData_1_1ContextTlv.html#a4be9b8bbcde6083724a07320f00aede0", null ]
    ] ],
    [ "CommissioningDataTlv", "d0/d15/classThread_1_1NetworkData_1_1CommissioningDataTlv.html", [
      [ "Init", "d0/d15/classThread_1_1NetworkData_1_1CommissioningDataTlv.html#a4f90032ec9250ce972df82f1e4a3b86f", null ]
    ] ]
];