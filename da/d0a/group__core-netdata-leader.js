var group__core_netdata_leader =
[
    [ "Leader", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html", [
      [ "Leader", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html#af0d5b5a867cd80bbc200c58c97c22b5e", null ],
      [ "GetCommissioningData", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html#ab1860ae9877c2b0f6a83d677d41e31d6", null ],
      [ "GetContext", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html#aeda6636db45b59e47b41830e19eb3676", null ],
      [ "GetContext", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html#a1b2353a4e3b28d869003c6cdabc56437", null ],
      [ "GetContextIdReuseDelay", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html#a077a8f039d961db6d8f0cf7fc01128aa", null ],
      [ "GetStableVersion", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html#af1d68084596bca7052e15217b2e96ccf", null ],
      [ "GetVersion", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html#a88ab46f99bc237628ebdc4153678ffca", null ],
      [ "IncrementStableVersion", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html#aaa82c03675af088d0d8c1dbb203b2029", null ],
      [ "IncrementVersion", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html#aef19c61f1f56ce22b1097c73590d5a83", null ],
      [ "IsOnMesh", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html#a0a137f0793d018f957e8e20513150ae1", null ],
      [ "RemoveBorderRouter", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html#a5baddd109ff06b58140e2678e3c97a16", null ],
      [ "Reset", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html#a631f664fa6b994e74b133b9761fae421", null ],
      [ "RouteLookup", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html#aa67c70e5397bc58a13d4afa77c54ff95", null ],
      [ "SendServerDataNotification", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html#a719450dae714bd29bfea19b7d1eb6e9f", null ],
      [ "SetCommissioningData", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html#a46d1a76edcb210f1cf58c463f3c1bdad", null ],
      [ "SetContextIdReuseDelay", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html#a11cd0f1181c3217bf39366fbc445f5e7", null ],
      [ "SetNetworkData", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html#a428d60927d6f4b4ac8e878e792d2f716", null ],
      [ "Start", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html#a7678c15cb9ad8817f478340a20f7069b", null ],
      [ "Stop", "d6/d9b/classThread_1_1NetworkData_1_1Leader.html#aed480933589287c7910778d4994917df", null ]
    ] ]
];