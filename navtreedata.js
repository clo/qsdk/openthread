var NAVTREE =
[
  [ "OpenThread", "index.html", [
    [ "Modules", "modules.html", "modules" ],
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", "functions_eval" ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html",
"d0/d95/ncp__spi_8cpp.html#af2dd08eddb534462aa96d769b7237654",
"d1/def/classThread_1_1MeshCoP_1_1Timestamp.html#a10afaf06203c87bf2b9295639bdce780",
"d3/d05/classThread_1_1Ip6_1_1Ip6.html#a4aa215eb5c0d575403b5ca683e8427c2",
"d4/d80/group__core-netdata-core.html#gae6fcdcf78495e36118524d8e858869ae",
"d5/d19/classThread_1_1Mac_1_1Frame.html#adeef9080d706ca2c0e64ddf7b66e0d90ad12b77026d872bb8e3c57ead3dcad5f5",
"d6/d83/classThread_1_1MeshCoP_1_1Dataset.html#a68ef44c6035a8e52d49f523226fbadfe",
"d7/d70/classThread_1_1NetworkDiagnostic_1_1MacCountersTlv.html#a12329e174312e132da25cbe67822cb01",
"d8/dd2/group__diags.html#ga0b52ad8f245c83734f5e8ad1bdde7f6f",
"d9/dcf/group__core-ip6-ip6.html#gga83d94b02a618038e1534f9e12001d095a6e7be0e85d196b143588cbae5b530ad8",
"da/d60/openthread_8cpp.html#gabbff7369b8794d597a5afd188e171ec0",
"db/d51/ip6__headers_8hpp.html",
"db/dec/group__core-mle-router.html#ga8ff435384ef5423683fbb74f0d87c1a9",
"dc/dbd/classThread_1_1Mac_1_1Beacon.html#a494f84956761acbd22e12352f79af2b5aadc1e7b98e71f6b0e125fe90a1e011c6",
"dd/d8d/classThread_1_1MeshCoP_1_1Tlv.html#ae7665134b0a5f9bf1bac5d92f94b723caffae15e5b99dfcca436459ce97ea0e77",
"de/df7/flash_8h.html#a932b47b025a64564c72f27e6bf4c5688",
"df/d4f/group__config-general.html#gga8c40a8d1b44523163be6dafa7d0ab2b4ae85cd3a7fe30ce3f2a9d26dc157c48c2",
"df/d94/group__core-mle-core.html#gabc154b6e95f5b169febcd7a582dfa1a3",
"functions_g.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';