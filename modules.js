var modules =
[
    [ "API", "d0/d2d/group__api.html", "d0/d2d/group__api" ],
    [ "Platform Abstraction", "de/d8b/group__platform.html", "de/d8b/group__platform" ],
    [ "Core", "d0/de1/group__core.html", "d0/de1/group__core" ],
    [ "Core-timer-trickle", "da/dc3/group__core-timer-trickle.html", "da/dc3/group__core-timer-trickle" ],
    [ "Core-netdiag", "db/d64/group__core-netdiag.html", "db/d64/group__core-netdiag" ],
    [ "Core-commissioning", "d7/d42/group__core-commissioning.html", "d7/d42/group__core-commissioning" ]
];