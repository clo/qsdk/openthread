var group__config_br =
[
    [ "otIp6Prefix", "d2/ddd/structotIp6Prefix.html", [
      [ "mLength", "d2/ddd/structotIp6Prefix.html#a4d4c2fecf041ebc7213e474381c49ebf", null ],
      [ "mPrefix", "d2/ddd/structotIp6Prefix.html#ad1f373d25097034f9d36631a1cffd30d", null ]
    ] ],
    [ "otBorderRouterConfig", "dd/dd0/structotBorderRouterConfig.html", [
      [ "mConfigure", "dd/dd0/structotBorderRouterConfig.html#a9fafbcb14f9369efeaf5d360fb8a1447", null ],
      [ "mDefaultRoute", "dd/dd0/structotBorderRouterConfig.html#a5a65e884930293c4a40c253524c56327", null ],
      [ "mDhcp", "dd/dd0/structotBorderRouterConfig.html#a4c51169916172340439b8c363dbb6968", null ],
      [ "mOnMesh", "dd/dd0/structotBorderRouterConfig.html#a1319bf366108cae969556d26cabc4cd2", null ],
      [ "mPreference", "dd/dd0/structotBorderRouterConfig.html#a581993094e5a7b6c1eb69bf6d5c55340", null ],
      [ "mPreferred", "dd/dd0/structotBorderRouterConfig.html#a2cbb73bd5a2311773b612bfbb2164bc2", null ],
      [ "mPrefix", "dd/dd0/structotBorderRouterConfig.html#a5c009110abe241276bf9cefb13e11822", null ],
      [ "mSlaac", "dd/dd0/structotBorderRouterConfig.html#acb3d9aa94909a591af0a48e93809160c", null ],
      [ "mStable", "dd/dd0/structotBorderRouterConfig.html#a6b0ab66d71377681702e00905e849f43", null ]
    ] ],
    [ "otExternalRouteConfig", "d4/d5d/structotExternalRouteConfig.html", [
      [ "mPreference", "d4/d5d/structotExternalRouteConfig.html#a617ae82b63a236a334fa17c40a918b12", null ],
      [ "mPrefix", "d4/d5d/structotExternalRouteConfig.html#ae4284a42a6559c9a0b8b1f60d9bb3349", null ],
      [ "mStable", "d4/d5d/structotExternalRouteConfig.html#aef6feede7f856cf9ac120777cb67209e", null ]
    ] ],
    [ "OT_NETWORK_DATA_ITERATOR_INIT", "db/da2/group__config-br.html#ga50b70c8de241d3444d91337ed7e0bac5", null ],
    [ "otBorderRouterConfig", "db/da2/group__config-br.html#ga021164642188962b5372ef7e49940581", null ],
    [ "otExternalRouteConfig", "db/da2/group__config-br.html#ga65d4d5b42c866bce653a878f189bf1b7", null ],
    [ "otIp6Prefix", "db/da2/group__config-br.html#ga7b2d93f4f2bc00e6086da230fcbbc44a", null ],
    [ "otNetworkDataIterator", "db/da2/group__config-br.html#ga741352633dff1a7aded3a8a2e9a25011", null ],
    [ "otAddBorderRouter", "db/da2/group__config-br.html#ga0390c654926088c2b8fdef3bec8cfa37", null ],
    [ "otAddExternalRoute", "db/da2/group__config-br.html#gadb015f3cec5745e5161788d0a2eabb23", null ],
    [ "otAddUnsecurePort", "db/da2/group__config-br.html#gaec4ff641d5b414d30b46435ba63bdb4f", null ],
    [ "otGetUnsecurePorts", "db/da2/group__config-br.html#gaa599a5ba370e49064f701db9f7c2f88f", null ],
    [ "otRemoveBorderRouter", "db/da2/group__config-br.html#gad7d4cbde69a071cecf2ccb473d2b2ebf", null ],
    [ "otRemoveExternalRoute", "db/da2/group__config-br.html#gae424b85a559043b4ae5a8b1846d68ced", null ],
    [ "otRemoveUnsecurePort", "db/da2/group__config-br.html#gaeac6d5f2e179d969ea87c640caad8b3d", null ],
    [ "otSendServerData", "db/da2/group__config-br.html#gaf9f7e49c190e8e11297d534e223d6274", null ]
];