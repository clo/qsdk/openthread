var structRadioPacket =
[
    [ "mChannel", "db/ddc/structRadioPacket.html#a63f62f71f7b1f303593d481cd9a5621b", null ],
    [ "mDidTX", "db/ddc/structRadioPacket.html#a809843fedbb64b7f7e94df3f97688e36", null ],
    [ "mLength", "db/ddc/structRadioPacket.html#acd9a1bcc1ad631cfe898b095de24c155", null ],
    [ "mLqi", "db/ddc/structRadioPacket.html#ae18d45197bf0349c1c583ef77c3ef9dc", null ],
    [ "mPower", "db/ddc/structRadioPacket.html#a3650fd4fc2c8ec45b68bf8731fe15ac1", null ],
    [ "mPsdu", "db/ddc/structRadioPacket.html#a7addcd8d0d334994dd97dddef155c1b0", null ],
    [ "mSecurityValid", "db/ddc/structRadioPacket.html#a86f7bcab2cd6a67db3379059a418a57c", null ]
];