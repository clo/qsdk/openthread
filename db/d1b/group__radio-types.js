var group__radio_types =
[
    [ "RadioPacket", "db/ddc/structRadioPacket.html", [
      [ "mChannel", "db/ddc/structRadioPacket.html#a63f62f71f7b1f303593d481cd9a5621b", null ],
      [ "mDidTX", "db/ddc/structRadioPacket.html#a809843fedbb64b7f7e94df3f97688e36", null ],
      [ "mLength", "db/ddc/structRadioPacket.html#acd9a1bcc1ad631cfe898b095de24c155", null ],
      [ "mLqi", "db/ddc/structRadioPacket.html#ae18d45197bf0349c1c583ef77c3ef9dc", null ],
      [ "mPower", "db/ddc/structRadioPacket.html#a3650fd4fc2c8ec45b68bf8731fe15ac1", null ],
      [ "mPsdu", "db/ddc/structRadioPacket.html#a7addcd8d0d334994dd97dddef155c1b0", null ],
      [ "mSecurityValid", "db/ddc/structRadioPacket.html#a86f7bcab2cd6a67db3379059a418a57c", null ]
    ] ],
    [ "otRadioCaps", "db/d1b/group__radio-types.html#gaaba4bf6660fc977d5f260bbc4759db49", null ],
    [ "PhyState", "db/d1b/group__radio-types.html#gae4fc9607be8fe78429890574e85da2e1", null ],
    [ "RadioPacket", "db/d1b/group__radio-types.html#ga4ec1493a92b716164820cfc6f63d0727", [
      [ "kMaxPHYPacketSize", "db/d1b/group__radio-types.html#gga4d29ca5db06e2ae647d1ec22548a9d2aa7814f5ab606236acae270c705e38304c", null ],
      [ "kPhyMinChannel", "db/d1b/group__radio-types.html#gga4d29ca5db06e2ae647d1ec22548a9d2aaac68aa0095ba8fbe62f4399af20f356a", null ],
      [ "kPhyMaxChannel", "db/d1b/group__radio-types.html#gga4d29ca5db06e2ae647d1ec22548a9d2aa689ec26cf2ac6ae2817bf368fe7df96f", null ],
      [ "kPhySupportedChannelMask", "db/d1b/group__radio-types.html#gga4d29ca5db06e2ae647d1ec22548a9d2aa5a1f12f32a9eae0d14e32a1862d9ac90", null ],
      [ "kPhySymbolsPerOctet", "db/d1b/group__radio-types.html#gga4d29ca5db06e2ae647d1ec22548a9d2aae6f1637d0c6947bb3a2b034d31fc9051", null ],
      [ "kPhyBitRate", "db/d1b/group__radio-types.html#gga4d29ca5db06e2ae647d1ec22548a9d2aafe33924e8801f0b816fb9be77240f2e1", null ],
      [ "kPhyNoLqi", "db/d1b/group__radio-types.html#gga4d29ca5db06e2ae647d1ec22548a9d2aa095a69d0c6de326b526121b98031b3af", null ]
    ] ],
    [ "otRadioCaps", "db/d1b/group__radio-types.html#ga64d6a0cf11075e2ab0079c9d86c34b55", [
      [ "kRadioCapsNone", "db/d1b/group__radio-types.html#gga64d6a0cf11075e2ab0079c9d86c34b55af71ef7e6e8404c56664ad79012c2ae4c", null ],
      [ "kRadioCapsAckTimeout", "db/d1b/group__radio-types.html#gga64d6a0cf11075e2ab0079c9d86c34b55a7f68645873c299b0b509b591cece61d4", null ],
      [ "kRadioCapsEnergyScan", "db/d1b/group__radio-types.html#gga64d6a0cf11075e2ab0079c9d86c34b55ab5a631b2f466b87b4cd38d785a6f0a83", null ]
    ] ],
    [ "PhyState", "db/d1b/group__radio-types.html#gae1ff73e01c49ff0cb218e4efd1b01fbc", null ]
];