var structotNetifAddress =
[
    [ "mAddress", "d8/d1d/structotNetifAddress.html#af7ae93284e0d8cf84f7fe626107f331c", null ],
    [ "mNext", "d8/d1d/structotNetifAddress.html#a2114aeb81861bfbde4655e93c97b9374", null ],
    [ "mPreferredLifetime", "d8/d1d/structotNetifAddress.html#a8f2fd3fb0781d79fb7dd89c726bc5015", null ],
    [ "mPrefixLength", "d8/d1d/structotNetifAddress.html#a34ba4bb9f7701678558399d5bd2ef69f", null ],
    [ "mValidLifetime", "d8/d1d/structotNetifAddress.html#a3c6ddb105e92de60258fb35a76257564", null ]
];