var group__radio_operation =
[
    [ "otPlatDiagRadioReceiveDone", "d7/d9c/group__radio-operation.html#gaa0be6d07fec8fccc00da07a29d698d6d", null ],
    [ "otPlatDiagRadioTransmitDone", "d7/d9c/group__radio-operation.html#ga3554fa9c78ba169eb13483533ed9f20c", null ],
    [ "otPlatRadioAddSrcMatchExtEntry", "d7/d9c/group__radio-operation.html#ga3f24dc46cba2235b6dea0670df50a5c2", null ],
    [ "otPlatRadioAddSrcMatchShortEntry", "d7/d9c/group__radio-operation.html#ga5e1cf45dd77ef0608056f2e9cf992d8d", null ],
    [ "otPlatRadioClearSrcMatchExtEntries", "d7/d9c/group__radio-operation.html#ga3f50253b5f4847da0444a001e014ba65", null ],
    [ "otPlatRadioClearSrcMatchExtEntry", "d7/d9c/group__radio-operation.html#ga57238173bfc0ad9cce66e999f9ad3234", null ],
    [ "otPlatRadioClearSrcMatchShortEntries", "d7/d9c/group__radio-operation.html#ga95d8f1c200b49f481d117765ec7a66d4", null ],
    [ "otPlatRadioClearSrcMatchShortEntry", "d7/d9c/group__radio-operation.html#ga923d941e8a4d37be5d41de6be4fc275f", null ],
    [ "otPlatRadioDisable", "d7/d9c/group__radio-operation.html#ga5b14c6db562150d0a80c47ef278e5ab8", null ],
    [ "otPlatRadioEnable", "d7/d9c/group__radio-operation.html#gad2f1f79b0fe0bb9015fd996c723ed52e", null ],
    [ "otPlatRadioEnableSrcMatch", "d7/d9c/group__radio-operation.html#gabb1f75e02a0c91b2db8e1325e8575861", null ],
    [ "otPlatRadioEnergyScan", "d7/d9c/group__radio-operation.html#gae3b9ed222f4ad93e1d09e285b57be1ef", null ],
    [ "otPlatRadioEnergyScanDone", "d7/d9c/group__radio-operation.html#ga65b11fca984c76f2b4bc782cd1e4811a", null ],
    [ "otPlatRadioGetCaps", "d7/d9c/group__radio-operation.html#ga5a5999dadb6ac0ee70a69a960930f132", null ],
    [ "otPlatRadioGetPromiscuous", "d7/d9c/group__radio-operation.html#gaef6191a2f5c0dd177dc009dae3796e8e", null ],
    [ "otPlatRadioGetRssi", "d7/d9c/group__radio-operation.html#ga4ac10bba73d06f59af623bbb3349d3fb", null ],
    [ "otPlatRadioGetTransmitBuffer", "d7/d9c/group__radio-operation.html#gafe1fdf44907fccdb06be0db517d4fb01", null ],
    [ "otPlatRadioIsEnabled", "d7/d9c/group__radio-operation.html#gaf26840dd6a3c6bd1da7156127c8306f5", null ],
    [ "otPlatRadioReceive", "d7/d9c/group__radio-operation.html#ga063a389dc7d1e68cbe495cc3cff758c1", null ],
    [ "otPlatRadioReceiveDone", "d7/d9c/group__radio-operation.html#ga75836c10d52472c0dfca98f490c4a5f7", null ],
    [ "otPlatRadioSetPromiscuous", "d7/d9c/group__radio-operation.html#ga0a905ad5c76c5931ee2ef395342f0dca", null ],
    [ "otPlatRadioSleep", "d7/d9c/group__radio-operation.html#ga979f5ee2e63f0cba66f372149b2ac013", null ],
    [ "otPlatRadioTransmit", "d7/d9c/group__radio-operation.html#ga23aa0adfee1ed815c98fc3c087700ed1", null ],
    [ "otPlatRadioTransmitDone", "d7/d9c/group__radio-operation.html#ga7d10a9bce7521f75bd84b32b979e1aa7", null ]
];