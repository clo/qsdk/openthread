var logging_windows_8h =
[
    [ "WPP_CONTROL_GUIDS", "d7/d95/logging-windows_8h.html#af9215df4a5adfe2e66e850036e7526b6", null ],
    [ "WPP_LEVEL_FLAGS__POST", "d7/d95/logging-windows_8h.html#abc8c1bbe194517e883570c556fe7b88a", null ],
    [ "WPP_LEVEL_FLAGS__PRE", "d7/d95/logging-windows_8h.html#affd97ea2829d51c5c4e8a329b5a5c0f2", null ],
    [ "WPP_LEVEL_FLAGS_ENABLED", "d7/d95/logging-windows_8h.html#a78036e3b4c998e108aaca52e8459bc0e", null ],
    [ "WPP_LEVEL_FLAGS_EXP_ENABLED", "d7/d95/logging-windows_8h.html#a2108bd82afeb4eb8a813f1bdca48e3c7", null ],
    [ "WPP_LEVEL_FLAGS_EXP_LOGGER", "d7/d95/logging-windows_8h.html#a91ae80f3b94b5e8c86a5a1a589a9eaf6", null ],
    [ "WPP_LEVEL_FLAGS_LOGGER", "d7/d95/logging-windows_8h.html#a41b266293ae01129a0ae6d24769cc53e", null ],
    [ "WPP_LEVEL_FLAGS_PRE", "d7/d95/logging-windows_8h.html#a16ff5dd93ba0cd77dc42ef185a4393d6", null ],
    [ "WPP_LOGIPV6", "d7/d95/logging-windows_8h.html#a7bd2dbd15855b0c5f2b638d06017774f", null ]
];