var classThread_1_1Crc16 =
[
    [ "Polynomial", "de/dd3/classThread_1_1Crc16.html#afc881177afd6b9e11182b83a3cef4653", [
      [ "kCcitt", "de/dd3/classThread_1_1Crc16.html#afc881177afd6b9e11182b83a3cef4653a04af28d27d18942d84d8daac46e7474f", null ],
      [ "kAnsi", "de/dd3/classThread_1_1Crc16.html#afc881177afd6b9e11182b83a3cef4653acaafe2c3487b205cf55d377d9a035c1e", null ]
    ] ],
    [ "Crc16", "de/dd3/classThread_1_1Crc16.html#aad0debba6eeaba0e983c3ee4cf01eb81", null ],
    [ "Get", "de/dd3/classThread_1_1Crc16.html#a2b5d1aeff2763a99c1046749c2693064", null ],
    [ "Init", "de/dd3/classThread_1_1Crc16.html#aabab245a38f45c0c7345a1a3b6ff7b07", null ],
    [ "Update", "de/dd3/classThread_1_1Crc16.html#a5854e8934d12a322c73ece960b1ad5f4", null ]
];