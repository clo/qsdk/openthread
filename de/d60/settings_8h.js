var settings_8h =
[
    [ "otPlatSettingsAbandonChange", "de/d60/settings_8h.html#aab881f0178b29b005cbd3be54a6fe716", null ],
    [ "otPlatSettingsAdd", "de/d60/settings_8h.html#a351c972556d23a89d0731a487867ffe4", null ],
    [ "otPlatSettingsBeginChange", "de/d60/settings_8h.html#aa6287b15b01a4ea381ce734e7287240d", null ],
    [ "otPlatSettingsCommitChange", "de/d60/settings_8h.html#a14fc43510f526b21b6f6e6301fa7dbc1", null ],
    [ "otPlatSettingsDelete", "de/d60/settings_8h.html#a54194f959334b8fb9dcfcb37f9e2b724", null ],
    [ "otPlatSettingsGet", "de/d60/settings_8h.html#ad2d11252b06cc30f5e7e5c6703d8dd21", null ],
    [ "otPlatSettingsInit", "de/d60/settings_8h.html#a1d59fced314c085fb42895fd6caad2c3", null ],
    [ "otPlatSettingsSet", "de/d60/settings_8h.html#a0ecea8c7aed4738774ec9183b7f54acc", null ],
    [ "otPlatSettingsWipe", "de/d60/settings_8h.html#ae5654bff3614b4c28f3a0714f9f1307f", null ]
];