var group__platform =
[
    [ "Alarm", "dd/db6/group__alarm.html", "dd/db6/group__alarm" ],
    [ "Diag", "df/d93/group__diag.html", "df/d93/group__diag" ],
    [ "Logging", "d8/dff/group__logging.html", "d8/dff/group__logging" ],
    [ "Radio", "d0/d5b/group__radio.html", "d0/d5b/group__radio" ],
    [ "Random", "dd/d10/group__random.html", "dd/d10/group__random" ],
    [ "SPI Slave", "d1/d88/group__spi-slave.html", "d1/d88/group__spi-slave" ],
    [ "Toolchain", "d7/da1/group__toolchain.html", "d7/da1/group__toolchain" ],
    [ "UART", "db/def/group__uart.html", "db/def/group__uart" ]
];