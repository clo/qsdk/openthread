var group__udp =
[
    [ "otSockAddr", "d3/d28/structotSockAddr.html", [
      [ "mAddress", "d3/d28/structotSockAddr.html#a10d1d646a9b527001349ca83b782cefc", null ],
      [ "mPort", "d3/d28/structotSockAddr.html#ad9b52e6e0906eb7727373a26412a1715", null ],
      [ "mScopeId", "d3/d28/structotSockAddr.html#a5d270a107da02908c99f17e22ad70e9a", null ]
    ] ],
    [ "otMessageInfo", "dd/d76/structotMessageInfo.html", [
      [ "mHopLimit", "dd/d76/structotMessageInfo.html#a6f2c260a3babcee555f67ac4c5feccc7", null ],
      [ "mInterfaceId", "dd/d76/structotMessageInfo.html#a04648c6a181c3dcb3c079f759b0c4cdc", null ],
      [ "mLinkInfo", "dd/d76/structotMessageInfo.html#a6b3a08bac516510224d957bc26d22992", null ],
      [ "mPeerAddr", "dd/d76/structotMessageInfo.html#a44b97257686b6f5fba5afa13b05cc259", null ],
      [ "mPeerPort", "dd/d76/structotMessageInfo.html#ac8684bbbf2e414822ccf9b3e5751527b", null ],
      [ "mSockAddr", "dd/d76/structotMessageInfo.html#a2fcb46a2a2e321c92c519035d72944e4", null ],
      [ "mSockPort", "dd/d76/structotMessageInfo.html#a2edb568cfed99202b24d66dfe786552c", null ]
    ] ],
    [ "otUdpSocket", "da/dfd/structotUdpSocket.html", [
      [ "mContext", "da/dfd/structotUdpSocket.html#a1988824d212b26a9b3f9cb5d412121a5", null ],
      [ "mHandler", "da/dfd/structotUdpSocket.html#aa2ecdf82a3d6a9b21e301314de2271f0", null ],
      [ "mNext", "da/dfd/structotUdpSocket.html#af59a47912e318de493cb93710638e25d", null ],
      [ "mPeerName", "da/dfd/structotUdpSocket.html#aa853c7bac2584475eeee492aaad610ca", null ],
      [ "mSockName", "da/dfd/structotUdpSocket.html#a4a99b2565fbe000fc93add20178374bd", null ],
      [ "mTransport", "da/dfd/structotUdpSocket.html#a50f748fd43d72b227196c764247cdd6b", null ]
    ] ],
    [ "otMessageInfo", "d2/d11/group__udp.html#ga626382fa8118ffa6fed7d9dab7439a96", null ],
    [ "otSockAddr", "d2/d11/group__udp.html#ga966fff91ccb31448707035a32ae4fb76", null ],
    [ "otUdpReceive", "d2/d11/group__udp.html#gacff67ed83068976da50fdfd6efdeaa1f", null ],
    [ "otUdpSocket", "d2/d11/group__udp.html#ga2d245018891da475518da5c375beeba7", null ],
    [ "otBindUdpSocket", "d2/d11/group__udp.html#gabbff7369b8794d597a5afd188e171ec0", null ],
    [ "otCloseUdpSocket", "d2/d11/group__udp.html#ga2957568d7098fc442abaadad77daabee", null ],
    [ "otNewUdpMessage", "d2/d11/group__udp.html#ga830b1fac9ad667bbedc0e68da91e70aa", null ],
    [ "otOpenUdpSocket", "d2/d11/group__udp.html#gaa44b7d374d87e6f3624c275913676954", null ],
    [ "otSendDiagnosticGet", "d2/d11/group__udp.html#ga4198c5e5356efec22801439076076a9f", null ],
    [ "otSendDiagnosticReset", "d2/d11/group__udp.html#ga6f3d90ff7fcd39b3767b1ebc8eb58e1a", null ],
    [ "otSendUdp", "d2/d11/group__udp.html#gaafd3057272c0b42c69aa7d35956f5c92", null ]
];