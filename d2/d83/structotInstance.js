var structotInstance =
[
    [ "otInstance", "d2/d83/structotInstance.html#a4565155344db34db80c9ecde6254bc59", null ],
    [ "mActiveScanCallback", "d2/d83/structotInstance.html#aab469aa77349b1041059038d8ace359d", null ],
    [ "mActiveScanCallbackContext", "d2/d83/structotInstance.html#a5f9f0944938d2e7e9059ca9c0d80fc66", null ],
    [ "mDiscoverCallback", "d2/d83/structotInstance.html#abc895918b13a8b42e9241f86310f243b", null ],
    [ "mDiscoverCallbackContext", "d2/d83/structotInstance.html#af674728464f8f48c2161389d47b9dd9f", null ],
    [ "mEnergyScanCallback", "d2/d83/structotInstance.html#a4752dc684a8795d6c1c41b509b63a7a7", null ],
    [ "mEnergyScanCallbackContext", "d2/d83/structotInstance.html#a4816b56763d0afadb1adda5d56b57ec6", null ],
    [ "mIp6", "d2/d83/structotInstance.html#a01c5941edfbc047ae8e10908c8b0b988", null ],
    [ "mMbedTls", "d2/d83/structotInstance.html#af2b88cfc9f002d7d564ee0375443f73b", null ],
    [ "mNetifCallback", "d2/d83/structotInstance.html#a4f65cabf1171a2f3b7590ce1008d7991", null ],
    [ "mReceiveIp6DatagramCallback", "d2/d83/structotInstance.html#aefcc5a6414a127afce85d255f23e7f34", null ],
    [ "mReceiveIp6DatagramCallbackContext", "d2/d83/structotInstance.html#a8a08211a4eab14d633dee37903c83995", null ],
    [ "mThreadNetif", "d2/d83/structotInstance.html#ae45ff45c6af4a31fe4eb4ccae64fb833", null ]
];