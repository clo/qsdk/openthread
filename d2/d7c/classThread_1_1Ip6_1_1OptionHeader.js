var classThread_1_1Ip6_1_1OptionHeader =
[
    [ "Action", "d2/d7c/classThread_1_1Ip6_1_1OptionHeader.html#a11a3e79ca8512121fa1bdf7a2d228339", [
      [ "kActionSkip", "d2/d7c/classThread_1_1Ip6_1_1OptionHeader.html#a11a3e79ca8512121fa1bdf7a2d228339a81e601322b14f1268228258b2740d5a7", null ],
      [ "kActionDiscard", "d2/d7c/classThread_1_1Ip6_1_1OptionHeader.html#a11a3e79ca8512121fa1bdf7a2d228339a47ec4173b32e8adbde9a86910b89b132", null ],
      [ "kActionForceIcmp", "d2/d7c/classThread_1_1Ip6_1_1OptionHeader.html#a11a3e79ca8512121fa1bdf7a2d228339a8054025b9f754513b146c6a259a7b81b", null ],
      [ "kActionIcmp", "d2/d7c/classThread_1_1Ip6_1_1OptionHeader.html#a11a3e79ca8512121fa1bdf7a2d228339ac174ee773ae826aca987554b2431e01a", null ],
      [ "kActionMask", "d2/d7c/classThread_1_1Ip6_1_1OptionHeader.html#a11a3e79ca8512121fa1bdf7a2d228339a7b864730330b4daffd5b889dfe52cdf3", null ]
    ] ],
    [ "GetAction", "d2/d7c/classThread_1_1Ip6_1_1OptionHeader.html#ae5d9638cee80149e0a3f2116aba422ff", null ],
    [ "GetLength", "d2/d7c/classThread_1_1Ip6_1_1OptionHeader.html#ac284bc7a465a12b3a2ac20927ecda327", null ],
    [ "GetType", "d2/d7c/classThread_1_1Ip6_1_1OptionHeader.html#a5526d213a992d14362a9eb8387a4403f", null ],
    [ "SetLength", "d2/d7c/classThread_1_1Ip6_1_1OptionHeader.html#aaa4be8de89f592ab4aa5a260e43fa083", null ],
    [ "SetType", "d2/d7c/classThread_1_1Ip6_1_1OptionHeader.html#ad60561b7bb2228c9636d7d1b6b4e18c2", null ]
];