var indexSectionsWithContent =
{
  0: "6abcdefghijklmnoprstuvw~",
  1: "abcdefhijklmnoprstuvw",
  2: "t",
  3: "acdefghijklmnoprstu",
  4: "abcdefghijklmnoprstuw~",
  5: "im",
  6: "abcdefhimoprst",
  7: "acdhimopst",
  8: "ko",
  9: "o",
  10: "6abcdegilmnoprstu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "defines",
  10: "groups"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Macros",
  10: "Modules"
};

