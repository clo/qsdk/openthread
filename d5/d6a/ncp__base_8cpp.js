var ncp__base_8cpp =
[
    [ "IGNORE_RETURN_VALUE", "d5/d6a/ncp__base_8cpp.html#ad075d21d63724204341f6caac622cbeb", null ],
    [ "NCP_PLAT_RESET_REASON", "d5/d6a/ncp__base_8cpp.html#a4203a5fce758950a5a25a42b790ac901", null ],
    [ "RSSI_OVERRIDE_DISABLED", "d5/d6a/ncp__base_8cpp.html#a6f83b3ac940ab53527f240a88e66ff31", null ],
    [ "kThreadMode_RxOnWhenIdle", "d5/d6a/ncp__base_8cpp.html#ad35116dc02b242c9cc375b15f47d5f53a94bed35b6cab1f3e7e9240ddb9cc35dd", null ],
    [ "kThreadMode_SecureDataRequest", "d5/d6a/ncp__base_8cpp.html#ad35116dc02b242c9cc375b15f47d5f53a171d22981ac59dc013e9dfa3c2ab5595", null ],
    [ "kThreadMode_FullFunctionDevice", "d5/d6a/ncp__base_8cpp.html#ad35116dc02b242c9cc375b15f47d5f53a81e3151c2b229bbffe4c49b8694752c2", null ],
    [ "kThreadMode_FullNetworkData", "d5/d6a/ncp__base_8cpp.html#ad35116dc02b242c9cc375b15f47d5f53acbe7b1ab584c37b5588cbd15abecacb3", null ],
    [ "BorderRouterConfigToFlagByte", "d5/d6a/ncp__base_8cpp.html#a7e6cd70da9618e6a94061eb74c8099d7", null ],
    [ "otNcpStreamWrite", "d5/d6a/ncp__base_8cpp.html#a525ce21ede544e3c8743cf8cdb96e20c", null ],
    [ "ResetReasonToSpinelStatus", "d5/d6a/ncp__base_8cpp.html#a92d22e6e6bab401e3f492353bb112b9b", null ],
    [ "ThreadErrorToSpinelStatus", "d5/d6a/ncp__base_8cpp.html#ae400b49b7377bd07578d0b583bacda29", null ],
    [ "sNcpContext", "d5/d6a/ncp__base_8cpp.html#af15fb10eb5e96e805f87175d48956406", null ]
];