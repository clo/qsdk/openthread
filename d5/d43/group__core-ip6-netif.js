var group__core_ip6_netif =
[
    [ "LinkAddress", "d6/d94/classThread_1_1Ip6_1_1LinkAddress.html", [
      [ "HardwareType", "d6/d94/classThread_1_1Ip6_1_1LinkAddress.html#a8f514bf71bd0eed7470a99f41ea3076c", [
        [ "kEui64", "d6/d94/classThread_1_1Ip6_1_1LinkAddress.html#a8f514bf71bd0eed7470a99f41ea3076cad88dd985fc468350fac9c879e370854b", null ]
      ] ],
      [ "mExtAddress", "d6/d94/classThread_1_1Ip6_1_1LinkAddress.html#a33235e08cf8f726a110d534ab9583d61", null ],
      [ "mLength", "d6/d94/classThread_1_1Ip6_1_1LinkAddress.html#aa7b30e9d9e7a978a3d37e8bf76747d06", null ],
      [ "mType", "d6/d94/classThread_1_1Ip6_1_1LinkAddress.html#a3a7b8a84a8c08bdc990a8359bc89759b", null ]
    ] ],
    [ "NetifUnicastAddress", "d9/d13/classThread_1_1Ip6_1_1NetifUnicastAddress.html", [
      [ "GetAddress", "d9/d13/classThread_1_1Ip6_1_1NetifUnicastAddress.html#a2bb426bd29e271bd1fc09b61d1044911", null ],
      [ "GetAddress", "d9/d13/classThread_1_1Ip6_1_1NetifUnicastAddress.html#adc28c9d546dc4ef42404f57c9ab639d6", null ],
      [ "GetNext", "d9/d13/classThread_1_1Ip6_1_1NetifUnicastAddress.html#ac7f38e1322075b0e6dfc7afc048f7eec", null ],
      [ "GetNext", "d9/d13/classThread_1_1Ip6_1_1NetifUnicastAddress.html#a7aa85abd06a10e1badd75acd7a90dee9", null ],
      [ "Netif", "d9/d13/classThread_1_1Ip6_1_1NetifUnicastAddress.html#a6886edba1ba2abded0d3013acc3551a0", null ]
    ] ],
    [ "NetifMulticastAddress", "d6/d94/classThread_1_1Ip6_1_1NetifMulticastAddress.html", [
      [ "GetAddress", "d6/d94/classThread_1_1Ip6_1_1NetifMulticastAddress.html#a10800bf39d15ffa0735e857c13e8d489", null ],
      [ "GetAddress", "d6/d94/classThread_1_1Ip6_1_1NetifMulticastAddress.html#aebea519115d355fbf20ddcaa3e3d46a3", null ],
      [ "GetNext", "d6/d94/classThread_1_1Ip6_1_1NetifMulticastAddress.html#a4a35d2af56f6d46864b0b72994f4348b", null ],
      [ "Netif", "d6/d94/classThread_1_1Ip6_1_1NetifMulticastAddress.html#a6886edba1ba2abded0d3013acc3551a0", null ]
    ] ],
    [ "NetifCallback", "d2/dfb/classThread_1_1Ip6_1_1NetifCallback.html", [
      [ "Free", "d2/dfb/classThread_1_1Ip6_1_1NetifCallback.html#a27a2d95d2a70ea07cb185fbc8f10cf28", null ],
      [ "IsFree", "d2/dfb/classThread_1_1Ip6_1_1NetifCallback.html#a62c27f48c8a83f2ae89b65d6518119d5", null ],
      [ "IsServing", "d2/dfb/classThread_1_1Ip6_1_1NetifCallback.html#a2d049c49132cb80c9681099af0181dfb", null ],
      [ "Set", "d2/dfb/classThread_1_1Ip6_1_1NetifCallback.html#a994d81f140fa4a62245e3336d37f0aaf", null ],
      [ "Netif", "d2/dfb/classThread_1_1Ip6_1_1NetifCallback.html#a6886edba1ba2abded0d3013acc3551a0", null ]
    ] ],
    [ "Netif", "d2/d55/classThread_1_1Ip6_1_1Netif.html", [
      [ "Netif", "d2/d55/classThread_1_1Ip6_1_1Netif.html#ab8f9918cf46154078aef7cbe418fec86", null ],
      [ "AddExternalUnicastAddress", "d2/d55/classThread_1_1Ip6_1_1Netif.html#ab186c9e859dd260cd8432fbebf7e2d67", null ],
      [ "AddUnicastAddress", "d2/d55/classThread_1_1Ip6_1_1Netif.html#ad891ec81750a2f7c4c722d07d7dc2632", null ],
      [ "GetInterfaceId", "d2/d55/classThread_1_1Ip6_1_1Netif.html#a6c8c4f1757a98e7a45197c97da1ec8b8", null ],
      [ "GetIp6", "d2/d55/classThread_1_1Ip6_1_1Netif.html#a9972d917c57200b504d9f2df7eb44974", null ],
      [ "GetLinkAddress", "d2/d55/classThread_1_1Ip6_1_1Netif.html#abdbe8ca62ef364c962b007c9a1f76a5e", null ],
      [ "GetName", "d2/d55/classThread_1_1Ip6_1_1Netif.html#a7f90a76c465da6ad23e53e8b2bf3aea3", null ],
      [ "GetNext", "d2/d55/classThread_1_1Ip6_1_1Netif.html#adc2577aa7e4aa3481587e8f075967246", null ],
      [ "GetUnicastAddresses", "d2/d55/classThread_1_1Ip6_1_1Netif.html#a5cc22cea2bfe4013bc67e03f379eb450", null ],
      [ "IsMulticastSubscribed", "d2/d55/classThread_1_1Ip6_1_1Netif.html#affa17002c66b0d80677e02c52b1802a8", null ],
      [ "IsStateChangedCallbackPending", "d2/d55/classThread_1_1Ip6_1_1Netif.html#adf6eb171189f05e7b53a1d49b18f5247", null ],
      [ "IsUnicastAddress", "d2/d55/classThread_1_1Ip6_1_1Netif.html#a585a20e652acf2053b83aa05de8bfa60", null ],
      [ "RegisterCallback", "d2/d55/classThread_1_1Ip6_1_1Netif.html#aca4693ab30ec940aef090d8228ea4429", null ],
      [ "RemoveCallback", "d2/d55/classThread_1_1Ip6_1_1Netif.html#a80610ecf1c8320603df9630427a29873", null ],
      [ "RemoveExternalUnicastAddress", "d2/d55/classThread_1_1Ip6_1_1Netif.html#a9ba666b91ccb367ede10cc95d696237e", null ],
      [ "RemoveUnicastAddress", "d2/d55/classThread_1_1Ip6_1_1Netif.html#ad00e58b94ab00cc71a5f3a242b7ef42c", null ],
      [ "RouteLookup", "d2/d55/classThread_1_1Ip6_1_1Netif.html#a0d4636e6aaf51eb31e4255b4610c36cd", null ],
      [ "SendMessage", "d2/d55/classThread_1_1Ip6_1_1Netif.html#a04fcb7cd1cc4b9ace37bdf07c233ed9c", null ],
      [ "SetStateChangedFlags", "d2/d55/classThread_1_1Ip6_1_1Netif.html#a092484addf28568306ab45941abffff4", null ],
      [ "SubscribeAllRoutersMulticast", "d2/d55/classThread_1_1Ip6_1_1Netif.html#a11dd43f622fa76425c1f3417a13b5e2d", null ],
      [ "SubscribeMulticast", "d2/d55/classThread_1_1Ip6_1_1Netif.html#a003983e22fecc0286e000269285c2e0c", null ],
      [ "UnsubscribeAllRoutersMulticast", "d2/d55/classThread_1_1Ip6_1_1Netif.html#a1c723e724a09fee4bda9c4ed28c7cd36", null ],
      [ "UnsubscribeMulticast", "d2/d55/classThread_1_1Ip6_1_1Netif.html#a0226c3af48eaa3fcc8014bcbee37aeb6", null ],
      [ "Ip6", "d2/d55/classThread_1_1Ip6_1_1Netif.html#ab5482934efe905a418522b0e40b35332", null ],
      [ "mIp6", "d2/d55/classThread_1_1Ip6_1_1Netif.html#a659a729e7065f4007e450463a0dfd63e", null ]
    ] ]
];