var structThread_1_1Coap_1_1Header_1_1Option =
[
    [ "kOptionDeltaOffset", "d1/d84/structThread_1_1Coap_1_1Header_1_1Option.html#ae1e3e4cb2ec191342455bcc18a296a79a92b0b438b1d379d3d38886a8ccbc014e", null ],
    [ "kOptionDeltaMask", "d1/d84/structThread_1_1Coap_1_1Header_1_1Option.html#ae1e3e4cb2ec191342455bcc18a296a79ab371bb9b10e928846f74a1a42b3dd2fb", null ],
    [ "Type", "d1/d84/structThread_1_1Coap_1_1Header_1_1Option.html#a8a5e73d0ad2efffd4de813a1fdd40089", [
      [ "kOptionUriPath", "d1/d84/structThread_1_1Coap_1_1Header_1_1Option.html#a8a5e73d0ad2efffd4de813a1fdd40089a0e65e3e14b86582767797f3432d0b3b6", null ],
      [ "kOptionContentFormat", "d1/d84/structThread_1_1Coap_1_1Header_1_1Option.html#a8a5e73d0ad2efffd4de813a1fdd40089a83e06f1e67155c91d81e55945adb10b1", null ]
    ] ],
    [ "mLength", "d1/d84/structThread_1_1Coap_1_1Header_1_1Option.html#a564f8d7e4377cef0c17cad9c57e5c3ec", null ],
    [ "mNumber", "d1/d84/structThread_1_1Coap_1_1Header_1_1Option.html#ac3b15830d02c0536252044812e459bcb", null ],
    [ "mValue", "d1/d84/structThread_1_1Coap_1_1Header_1_1Option.html#a06c4b7697acd4559de7c5790395368ae", null ]
];