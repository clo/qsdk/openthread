var misc_8h =
[
    [ "otPlatResetReason", "d4/d08/misc_8h.html#a0cba84283c1ab3c6aa37c143e9a552c8", [
      [ "kPlatResetReason_PowerOn", "d4/d08/misc_8h.html#a0cba84283c1ab3c6aa37c143e9a552c8a82b784469366da574b4fdc23e8a27963", null ],
      [ "kPlatResetReason_External", "d4/d08/misc_8h.html#a0cba84283c1ab3c6aa37c143e9a552c8a7deed152fa2edb473f56a88231e3b12d", null ],
      [ "kPlatResetReason_Software", "d4/d08/misc_8h.html#a0cba84283c1ab3c6aa37c143e9a552c8a1f0da3b789ff2aa8b3d003a2c50dc9b6", null ],
      [ "kPlatResetReason_Fault", "d4/d08/misc_8h.html#a0cba84283c1ab3c6aa37c143e9a552c8a8050e859ef62b4915e010aa88aea29f0", null ],
      [ "kPlatResetReason_Crash", "d4/d08/misc_8h.html#a0cba84283c1ab3c6aa37c143e9a552c8aa2cbbed149d09e3d05cf2ec9a7425388", null ],
      [ "kPlatResetReason_Assert", "d4/d08/misc_8h.html#a0cba84283c1ab3c6aa37c143e9a552c8a3047eaf89b9b013e063eae6bc371bbbd", null ],
      [ "kPlatResetReason_Other", "d4/d08/misc_8h.html#a0cba84283c1ab3c6aa37c143e9a552c8a79eaa88ba77470f3a56b049f59c46513", null ],
      [ "kPlatResetReason_Unknown", "d4/d08/misc_8h.html#a0cba84283c1ab3c6aa37c143e9a552c8ad4d67cac224e03ecbd999ce82e0e5a3e", null ],
      [ "kPlatResetReason_Watchdog", "d4/d08/misc_8h.html#a0cba84283c1ab3c6aa37c143e9a552c8ade0fa6b88a275f37763bbe14f4155cfc", null ],
      [ "kPlatResetReason_Count", "d4/d08/misc_8h.html#a0cba84283c1ab3c6aa37c143e9a552c8a3b60f43e7653f1e5458ba7a2261f6a25", null ]
    ] ],
    [ "otPlatGetResetReason", "d4/d08/misc_8h.html#ab26ce83d07357b4c94234a5c12aff289", null ],
    [ "otPlatReset", "d4/d08/misc_8h.html#a6e5e33121dff7942b201db3cc37e9e86", null ]
];