var classThread_1_1ThreadStatusTlv =
[
    [ "Status", "d4/df4/classThread_1_1ThreadStatusTlv.html#a0a1376dc8bb01111ac1a21170188fdf8", [
      [ "kSuccess", "d4/df4/classThread_1_1ThreadStatusTlv.html#a0a1376dc8bb01111ac1a21170188fdf8a5fb89c5f6001daef30afa2028aad4bd5", null ],
      [ "kNoAddressAvailable", "d4/df4/classThread_1_1ThreadStatusTlv.html#a0a1376dc8bb01111ac1a21170188fdf8ace6de0c3cc698f5e4fd3c429e16321ad", null ],
      [ "kTooFewRouters", "d4/df4/classThread_1_1ThreadStatusTlv.html#a0a1376dc8bb01111ac1a21170188fdf8ad7b63b69f1aab8dac7c687217299815e", null ],
      [ "kHaveChildIdRequest", "d4/df4/classThread_1_1ThreadStatusTlv.html#a0a1376dc8bb01111ac1a21170188fdf8a693d46178b2f2c12c0df6afec605b6b3", null ]
    ] ],
    [ "GetStatus", "d4/df4/classThread_1_1ThreadStatusTlv.html#a1f78a16547215b763f9ba4b2c4c2067e", null ],
    [ "Init", "d4/df4/classThread_1_1ThreadStatusTlv.html#a249b814440f54492a04513f4e04f5212", null ],
    [ "IsValid", "d4/df4/classThread_1_1ThreadStatusTlv.html#a1417d5b6a95eef2e204161dd310795d7", null ],
    [ "SetStatus", "d4/df4/classThread_1_1ThreadStatusTlv.html#a4703f4632a623065dfbfc29e24f8ea67", null ]
];