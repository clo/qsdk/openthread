var ncp__spi_8cpp =
[
    [ "SPI_CRC_FLAG", "d0/d95/ncp__spi_8cpp.html#af6ca5a8d4f5370c6b5cb5550dc360d9c", null ],
    [ "SPI_PATTERN_MASK", "d0/d95/ncp__spi_8cpp.html#ab5e5cffbfeb58a763fe550d1f0f0b91f", null ],
    [ "SPI_PATTERN_VALUE", "d0/d95/ncp__spi_8cpp.html#a8d9cb330693fddd3c52e4323a1515926", null ],
    [ "SPI_RESET_FLAG", "d0/d95/ncp__spi_8cpp.html#aa27409ae81b13488841070a924cb3c83", null ],
    [ "otDEFINE_ALIGNED_VAR", "d0/d95/ncp__spi_8cpp.html#a70df1e8bee8044c29bc04df00e19658c", null ],
    [ "otNcpInit", "d0/d95/ncp__spi_8cpp.html#abe9c8db53c99d46aa0f96fe8dde53e05", null ],
    [ "spi_header_get_accept_len", "d0/d95/ncp__spi_8cpp.html#adffca4f9fc8ec2c07226ddcd5282efe3", null ],
    [ "spi_header_get_data_len", "d0/d95/ncp__spi_8cpp.html#af2dd08eddb534462aa96d769b7237654", null ],
    [ "spi_header_get_flag_byte", "d0/d95/ncp__spi_8cpp.html#a6112a7fe1e18d86eb13cd2f6aec7dac8", null ],
    [ "spi_header_set_accept_len", "d0/d95/ncp__spi_8cpp.html#a2eda884a0775883edc211dda6f184837", null ],
    [ "spi_header_set_data_len", "d0/d95/ncp__spi_8cpp.html#a012e4e364819a664b1ad7edb10c49e2c", null ],
    [ "spi_header_set_flag_byte", "d0/d95/ncp__spi_8cpp.html#a06e047fba63071fc72fdeaf5946aff23", null ],
    [ "sNcpSpi", "d0/d95/ncp__spi_8cpp.html#a205b5fb6486fa7638e71407606a01d39", null ]
];